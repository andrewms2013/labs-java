import org.javatuples.Pair;

public class Circle {
    public Circle(double xCord, double yCord, double radius) {
        this.coordinates = new Pair<Double, Double>(xCord, yCord);
        this.radius = radius;
    }

    private Pair<Double, Double> coordinates;
    private double radius;

    public double calculateSquare() {
        return Math.PI * Math.pow(radius, 2);
    }
}
