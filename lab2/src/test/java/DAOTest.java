import com.kpi.ua.lab2.connection.PostgresDataSource;
import com.kpi.ua.lab2.dao.DAO;
import com.kpi.ua.lab2.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DAOTest {

    @Mock
    private PostgresDataSource dataSource;

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement statement;

    @Mock
    private ResultSet resultSet;

    @BeforeEach
    void beforeEach() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);
    }

    @Test
    public void getDogTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true,  false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l);
        when(resultSet.getDate(eq("birthdate"))).thenReturn(birthdate);
        when(resultSet.getLong(eq("owner_id"))).thenReturn(12l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob");
        when(resultSet.getString(eq("animal_passport_id"))).thenReturn("AC123456");
        when(resultSet.getBoolean(eq("is_good_boy"))).thenReturn(true);
        Dog animal = new Dog(12l, birthdate, 12l, "Bob", "AC123456", true);
        assertEquals(animal.getIsGoodBoy(), dao.getDog(12l).getIsGoodBoy());
    }

    @Test
    void getDogListTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l, 13l);
        when(resultSet.getDate(eq("birthdate"))).thenReturn(birthdate, birthdate);
        when(resultSet.getLong(eq("owner_id"))).thenReturn(12l, 12l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob", "Dob");
        when(resultSet.getString(eq("animal_passport_id"))).thenReturn("AC123456", "AC123457");
        when(resultSet.getBoolean(eq("is_good_boy"))).thenReturn(true, true);
        List<Dog> list = dao.getDogList();
        List<Dog> expectedList = new ArrayList<Dog>() {{
            add(new Dog(12l, birthdate, 12l, "Bob",  "AC123456", true));
            add(new Dog(13l, birthdate, 12l, "Dob",  "AC123457", true));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getAnimalPassportId(), expectedList.get(i).getAnimalPassportId());
        }
    }


    @Test
    public void getCatTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l);
        when(resultSet.getDate(eq("birthdate"))).thenReturn(birthdate);
        when(resultSet.getLong(eq("owner_id"))).thenReturn(12l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob");
        when(resultSet.getString(eq("animal_passport_id"))).thenReturn("AC123456");
        when(resultSet.getBoolean(eq("is_good_cat"))).thenReturn(true);
        Cat animal = new Cat(12l, birthdate, 12l, "Bob", "AC123456", true);
        assertEquals(animal.getBirthdate(), dao.getCat(12l).getBirthdate());
    }

    @Test
    void getCatListTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l, 13l);
        when(resultSet.getDate(eq("birthdate"))).thenReturn(birthdate, birthdate);
        when(resultSet.getLong(eq("owner_id"))).thenReturn(12l, 12l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob", "Dob");
        when(resultSet.getString(eq("animal_passport_id"))).thenReturn("AC123456", "AC123457");
        when(resultSet.getBoolean(eq("is_good_cat"))).thenReturn(true, true);
        List <Cat> list = dao.getCatList();
        List <Cat> expectedList = new ArrayList<Cat>() {{
            add(new Cat(12l, birthdate, 12l, "Bob",  "AC123456", true));
            add(new Cat(13l, birthdate, 12l, "Dob",  "AC123456", true));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getIsGoodCat(), expectedList.get(i).getIsGoodCat());
        }
    }

    @Test
    public void getClientTest() throws Exception {
        DAO dao = new DAO(dataSource);
        when(resultSet.next()).thenReturn(true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l);
        when(resultSet.getLong(eq("clinic_id"))).thenReturn(12l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob");
        when(resultSet.getString(eq("surname"))).thenReturn("Voizinsky");
        when(resultSet.getBoolean(eq("has_discount"))).thenReturn(true);
        Client client = new Client(12l, "Bob", "Voizinsky", 12l, true);
        Client receivedClient = dao.getClient(12l);
        assertEquals(client.getId(), receivedClient.getId());
        assertEquals(client.getName(), receivedClient.getName());
        assertEquals(client.getHasDiscount(), receivedClient.getHasDiscount());
    }

    @Test
    void getClientListTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l, 13l);
        when(resultSet.getLong(eq("clinic_id"))).thenReturn(12l, 11l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob", "Rob");
        when(resultSet.getString(eq("surname"))).thenReturn("Voizinsky", "Stark");
        when(resultSet.getBoolean(eq("has_discount"))).thenReturn(true, false);
        List<Client> list = dao.getClientList();
        List<Client> expectedList = new ArrayList<Client>() {{
            add(new Client(12l, "Bob", "Voizinsky", 12l, true));
            add(new Client(13l, "Rob", "Stark", 11l, false));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getId(), expectedList.get(i).getId());
            assertEquals(list.get(i).getName(), expectedList.get(i).getName());
            assertEquals(list.get(i).getHasDiscount(), expectedList.get(i).getHasDiscount());
        }
    }

    @Test
    public void getClinicTest() throws Exception {
        DAO dao = new DAO(dataSource);
        when(resultSet.next()).thenReturn(true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l);
        when(resultSet.getLong(eq("aviaries_quantity"))).thenReturn(12l);
        when(resultSet.getString(eq("city"))).thenReturn("Kyiv");
        when(resultSet.getString(eq("house"))).thenReturn("12-B");
        when(resultSet.getString(eq("street"))).thenReturn("Korolyova");
        Clinic clinic = new Clinic(12l, 12l, "Kyiv", "12-B", "Korolyova");
        Clinic receivedClinic = dao.getClinic(12l);
        assertEquals(clinic.getId(), receivedClinic.getId());
        assertEquals(clinic.getCity(), receivedClinic.getCity());
        assertEquals(clinic.getHouse(), receivedClinic.getHouse());
    }

    @Test
    void getClinicListTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l, 13l);
        when(resultSet.getLong(eq("aviaries_quantity"))).thenReturn(12l, 1l);
        when(resultSet.getString(eq("city"))).thenReturn("Kyiv", "Moskow");
        when(resultSet.getString(eq("house"))).thenReturn("12-B", "11");
        when(resultSet.getString(eq("street"))).thenReturn("Korolyova", "Saharova");
        List<Clinic> list = dao.getClinicList();
        List<Clinic> expectedList = new ArrayList<Clinic>() {{
            add(new Clinic(12l, 12l, "Kyiv", "12-B", "Korolyova"));
            add(new Clinic(13l, 1l, "Moskow", "11", "Saharova"));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getId(), expectedList.get(i).getId());
            assertEquals(list.get(i).getCity(), expectedList.get(i).getCity());
            assertEquals(list.get(i).getHouse(), expectedList.get(i).getHouse());
        }
    }

    @Test
    public void getDoctorTest() throws Exception {
        DAO dao = new DAO(dataSource);
        when(resultSet.next()).thenReturn(true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l);
        when(resultSet.getLong(eq("clinic_id"))).thenReturn(12l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob");
        when(resultSet.getString(eq("surname"))).thenReturn("Voizinsky");
        when(resultSet.getString(eq("speciality"))).thenReturn("Surgeon");
        when(resultSet.getString(eq("qualification"))).thenReturn("Masters");
        Doctor doctor = new Doctor(12l, "Bob", "Voizinsky", 12l, "Surgeon", "Masters");
        assertEquals(doctor.getId(), dao.getDoctor(12l).getId());
    }

    @Test
    void getDoctorListTest() throws Exception {
        DAO dao = new DAO(dataSource);
        Date birthdate = new Date(15, 10, 2000);
        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getLong(eq("id"))).thenReturn(12l, 13l);
        when(resultSet.getLong(eq("clinic_id"))).thenReturn(12l, 11l);
        when(resultSet.getString(eq("name"))).thenReturn("Bob", "Rob");
        when(resultSet.getString(eq("surname"))).thenReturn("Voizinsky", "Stark");
        when(resultSet.getString(eq("speciality"))).thenReturn("Surgeon", "Oculist");
        when(resultSet.getString(eq("qualification"))).thenReturn("Masters", "PhD");
        List<Doctor> list = dao.getDoctorList();
        List<Doctor> expectedList = new ArrayList<Doctor>() {{
            add(new Doctor(12l, "Bob", "Voizinsky", 12l, "Surgeon", "Masters"));
            add(new Doctor(13l, "Rob", "Stark", 11l, "Oculist", "PhD"));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i).getQualification(), expectedList.get(i).getQualification());
        }
    }


}
