package com.kpi.ua.lab2;

import com.kpi.ua.lab2.connection.IDataSource;
import com.kpi.ua.lab2.connection.PostgresDataSource;
import com.kpi.ua.lab2.dao.DAO;
import com.kpi.ua.lab2.model.Cat;
import com.kpi.ua.lab2.model.Client;
import org.postgresql.ds.PGPoolingDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try {
            Connection con = new PostgresDataSource().getConnection();
            Statement statement = con.createStatement();
            ResultSet set = statement.executeQuery("SELECT id, birthdate, owner_id, name, animal_passport_id FROM public.\"Animal\";");
            set.next();
            int id = set.getInt("id");
            String name = set.getString("name");
            System.out.println(id);
            System.out.println(name);
            IDataSource dataSource = new PostgresDataSource();
            DAO dao = new DAO(dataSource);
            List<Client> clients = dao.getClientList();
            System.out.println(clients.get(1).getId());
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
