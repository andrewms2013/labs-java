package com.kpi.ua.lab2.model;

import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;

import java.sql.Date;

@Entity(name = "Animal")
public abstract class Animal {
    @PrimaryKey()
    @ColumnName(name = "id")
    private Long id;
    @ColumnName(name = "birthdate")
    private Date birthdate;
    @ColumnName(name = "owner_id")
    private Long ownerId;
    @ColumnName(name = "name")
    private String name;
    @ColumnName(name = "animal_passport_id")
    private String animalPassportId;

    public Animal() { };

    public Animal(Long id, Date birthdate, Long ownerId, String name, String animalPassportId) {
        this.id = id;
        this.birthdate = birthdate;
        this.ownerId = ownerId;
        this.name = name;
        this.animalPassportId = animalPassportId;
    }

    public Long getId() {
        return id;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public String getName() {
        return name;
    }

    public String getAnimalPassportId() {
        return animalPassportId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnimalPassportId(String animalPassportId) {
        this.animalPassportId = animalPassportId;
    }
}
