package com.kpi.ua.lab2.model;

import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;

@Entity(name = "Doctor")
public class Doctor {
    @PrimaryKey()
    @ColumnName(name = "id")
    private Long id;
    @ColumnName(name = "name")
    private String name;
    @ColumnName(name = "surname")
    private String surname;
    @ColumnName(name = "clinic_id")
    private Long clinicId;
    @ColumnName(name = "speciality")
    private String speciality;
    @ColumnName(name = "qualification")
    private String qualification;

    public Doctor() { };

    public Doctor(Long id, String name, String surname, Long clinicId, String speciality, String qualification) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.clinicId = clinicId;
        this.speciality = speciality;
        this.qualification = qualification;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getClinicId() {
        return clinicId;
    }

    public void setClinicId(Long clinicId) {
        this.clinicId = clinicId;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }
}
