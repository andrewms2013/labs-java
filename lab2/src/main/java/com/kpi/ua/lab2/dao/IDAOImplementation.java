package com.kpi.ua.lab2.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface IDAOImplementation<T> {
    T getItem(Long Id) throws Exception;
    List<T> getEntityList() throws Exception;
}
