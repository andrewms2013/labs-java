package com.kpi.ua.lab2.connection;

import java.sql.Connection;

public interface IDataSource {
    public Connection getConnection();
}
