package com.kpi.ua.lab2.dao;

import com.kpi.ua.lab2.connection.IDataSource;
import com.kpi.ua.lab2.connection.PostgresDataSource;
import com.kpi.ua.lab2.model.*;

import java.util.List;

public class DAO implements IDAO {

    IDAOImplementation<Cat> catIDAOImplementation;
    IDAOImplementation<Dog> dogIDAOImplementation;
    IDAOImplementation<Client> clientIDAOImplementation;
    IDAOImplementation<Clinic> clinicIDAOImplementation;
    IDAOImplementation<Doctor> doctorIDAOImplementation;

    private IDataSource dataSource;

    public DAO(IDataSource dataSource) {
        this.dataSource = dataSource;
        catIDAOImplementation = new DAOImplementation<Cat>(dataSource, Cat.class);
        dogIDAOImplementation = new DAOImplementation<Dog>(dataSource, Dog.class);
        clinicIDAOImplementation = new DAOImplementation<Clinic>(dataSource, Clinic.class);
        clientIDAOImplementation = new DAOImplementation<Client>(dataSource, Client.class);
        doctorIDAOImplementation = new DAOImplementation<Doctor>(dataSource, Doctor.class);
    }

    @Override
    public Dog getDog(Long id) throws Exception {
        return dogIDAOImplementation.getItem(id);
    }

    @Override
    public List<Dog> getDogList() throws Exception {
        return dogIDAOImplementation.getEntityList();
    }

    @Override
    public Cat getCat(Long id) throws Exception { return catIDAOImplementation.getItem(id); }

    @Override
    public List<Cat> getCatList() throws Exception {
        return catIDAOImplementation.getEntityList();
    }

    @Override
    public Client getClient(Long id) throws Exception {
        return clientIDAOImplementation.getItem(id);
    }

    @Override
    public List<Client> getClientList() throws Exception {
        return clientIDAOImplementation.getEntityList();
    }

    @Override
    public Clinic getClinic(Long id) throws Exception {
        return clinicIDAOImplementation.getItem(id);
    }

    @Override
    public List<Clinic> getClinicList() throws Exception {
        return clinicIDAOImplementation.getEntityList();
    }

    @Override
    public Doctor getDoctor(Long id) throws Exception {
        return doctorIDAOImplementation.getItem(id);
    }

    @Override
    public List<Doctor> getDoctorList() throws Exception {
        return doctorIDAOImplementation.getEntityList();
    }
}
