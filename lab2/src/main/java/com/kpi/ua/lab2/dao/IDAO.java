package com.kpi.ua.lab2.dao;

import com.kpi.ua.lab2.model.*;

import java.util.List;

public interface IDAO {
    Cat getCat(Long id) throws Exception;
    List<Cat> getCatList() throws Exception;

    Dog getDog(Long id) throws Exception;
    List<Dog> getDogList() throws Exception;

    Client getClient(Long id) throws Exception;
    List<Client> getClientList() throws Exception;

    Clinic getClinic(Long id) throws Exception;
    List<Clinic> getClinicList() throws Exception;

    Doctor getDoctor(Long id) throws Exception;
    List<Doctor> getDoctorList() throws Exception;
}
