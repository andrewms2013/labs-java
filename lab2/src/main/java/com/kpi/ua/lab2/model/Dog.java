package com.kpi.ua.lab2.model;

import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;

import java.sql.Date;

@Entity(name = "Dog")
public class Dog extends Animal {
    @PrimaryKey()
    @ColumnName(name = "id")
    private long id;

    @ColumnName(name = "is_good_boy")
    private boolean isGoodBoy;

    public Dog() { };

    public Dog(Long id, Date birthdate, Long ownerId, String name, String animalPassportId, boolean isGoodBoy) {
        super(id, birthdate, ownerId, name, animalPassportId);
        this.isGoodBoy = isGoodBoy;
    }

    public boolean getIsGoodBoy() { return isGoodBoy; }

    public void setIsGoodBoy(boolean isGoodBoy) {
        this.isGoodBoy = isGoodBoy;
    }
}
