package com.kpi.ua.lab2.model;

import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;

@Entity(name = "Clinic")
public class Clinic {
    @PrimaryKey()
    @ColumnName(name = "id")
    private Long id;
    @ColumnName(name = "aviaries_quantity")
    private Long aviariesQuantity;
    @ColumnName(name = "city")
    private String city;
    @ColumnName(name = "house")
    private String house;
    @ColumnName(name = "street")
    private String street;

    public Clinic() { };

    public Clinic(Long id, Long aviariesQuantity, String city, String house, String street) {
        this.id = id;
        this.aviariesQuantity = aviariesQuantity;
        this.city = city;
        this.house = house;
        this.street = street;
    }

    public Long getId() {
        return id;
    }

    public Long getAviariesQuantity() {
        return aviariesQuantity;
    }

    public String getCity() {
        return city;
    }

    public String getHouse() {
        return house;
    }

    public String getStreet() {
        return street;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAviariesQuantity(Long aviariesQuantity) {
        this.aviariesQuantity = aviariesQuantity;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
