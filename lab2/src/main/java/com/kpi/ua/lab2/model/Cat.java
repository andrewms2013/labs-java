package com.kpi.ua.lab2.model;

import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;

import java.sql.Date;

@Entity(name = "Cat")
public class Cat extends Animal {
    @PrimaryKey()
    @ColumnName(name = "id")
    private long id;

    @ColumnName(name = "is_good_cat")
    private boolean isGoodCat;

    public Cat() { };

    public Cat(Long id, Date birthdate, Long ownerId, String name, String animalPassportId, boolean isGoodCat) {
        super(id, birthdate, ownerId, name, animalPassportId);
        this.isGoodCat = isGoodCat;
    }

    public boolean getIsGoodCat() {
        return isGoodCat;
    }

    public void setIsGoodCat(boolean isGoodCat) {
        this.isGoodCat = isGoodCat;
    }
}
