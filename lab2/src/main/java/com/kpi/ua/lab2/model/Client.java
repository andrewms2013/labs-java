package com.kpi.ua.lab2.model;

import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;

@Entity(name = "Client")
public class Client {
    @PrimaryKey()
    @ColumnName(name = "id")
    private Long id;
    @ColumnName(name = "name")
    private String name;
    @ColumnName(name = "surname")
    private String surname;
    @ColumnName(name = "clinic_id")
    private Long clinicId;
    @ColumnName(name = "has_discount")
    private boolean hasDiscount;

    public Client() { };

    public Client(Long id, String name, String surname, Long clinicId, boolean hasDiscount) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.clinicId = clinicId;
        this.hasDiscount = hasDiscount;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getClinicId() {
        return clinicId;
    }

    public void setClinicId(Long clinicId) {
        this.clinicId = clinicId;
    }

    public boolean getHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

}
