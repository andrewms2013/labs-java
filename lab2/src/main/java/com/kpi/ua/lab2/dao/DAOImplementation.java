package com.kpi.ua.lab2.dao;


import com.kpi.ua.lab2.annotations.ColumnName;
import com.kpi.ua.lab2.annotations.Entity;
import com.kpi.ua.lab2.annotations.PrimaryKey;
import com.kpi.ua.lab2.connection.IDataSource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class DAOImplementation<T> implements IDAOImplementation<T>{

    Class<T> clazz;
    private IDataSource dataSource;

    public DAOImplementation(IDataSource dataSource, Class<T> clazz) {
        this.clazz = clazz;
        this.dataSource = dataSource;
    }

    @Override
    public T getItem(Long id) throws Exception {
        List<String> namesOfTables = this.getListOfTableNamesForClasses(clazz);
        List<List<Field>> fields = this.getListOfFieldsForClasses(clazz);
        List<List<String>> namesOfColumns = this.getListOfColumnsForClasses(fields);
        List<String> primaryKeys = this.getPrimaryKeysForClasses(fields);
        String query = this.buildQuery(namesOfTables, namesOfColumns, primaryKeys, id);
        List<T> result = this.executeQueryAndGetObjects(query, fields, namesOfColumns, primaryKeys);
        if(result.size() == 0) {
            throw new Exception("No entity with such primary key found");
        }
        return result.get(0);
    }

    @Override
    public List<T> getEntityList() throws Exception {
        List<String> namesOfTables = this.getListOfTableNamesForClasses(clazz);
        List<List<Field>> fields = this.getListOfFieldsForClasses(clazz);
        List<List<String>> namesOfColumns = this.getListOfColumnsForClasses(fields);
        List<String> primaryKeys = this.getPrimaryKeysForClasses(fields);
        String query = this.buildQuery(namesOfTables, namesOfColumns, primaryKeys, null);
        List<T> result = this.executeQueryAndGetObjects(query, fields, namesOfColumns, primaryKeys);
        return result;
    }

    private List<List<Field>> getListOfFieldsForClasses(Class<?> clazz) {
        List<List<Field>> fields = new ArrayList<>();
        Class<?> currentClass = clazz;
        while (currentClass.isAnnotationPresent(Entity.class)) {
            List<Field> fieldsForCurrentClass = new ArrayList<>();
            fieldsForCurrentClass.addAll(Arrays.asList(currentClass.getDeclaredFields()));
            fields.add(fieldsForCurrentClass);
            currentClass = currentClass.getSuperclass();
        }
        return fields;
    }

    private List<List<String>> getListOfColumnsForClasses(List<List<Field>> fieldsForClasses) {
        List<List<String>> columnsForClasses = new ArrayList<>();
        for(List<Field> fieldsForClass: fieldsForClasses) {
            List<String> columnsForClass = new ArrayList<>();
            for(Field field: fieldsForClass) if (field.getAnnotation(ColumnName.class) != null) {
                columnsForClass.add(field.getAnnotation(ColumnName.class).name());
            }
            columnsForClasses.add(columnsForClass);
        }
        return columnsForClasses;
    }

    private List<String> getListOfTableNamesForClasses(Class<?> clazz) {
        List<String> tableNames = new ArrayList<>();
        Class<?> currentClass = clazz;
        while (currentClass.isAnnotationPresent(Entity.class)) {
            tableNames.add((currentClass.getAnnotation(Entity.class)).name());
            currentClass = currentClass.getSuperclass();
        }
        return tableNames;
    }

    private List<String> getPrimaryKeysForClasses(List<List<Field>> fieldsList) {
        List<String> primaryKeys = new ArrayList<>();
        for(List<Field> listOfFieldsForClass: fieldsList) {
            for(Field field: listOfFieldsForClass) if (field.getAnnotation(PrimaryKey.class) != null) {
                primaryKeys.add(field.getAnnotation(ColumnName.class).name());
                break;
            }
        }
        return primaryKeys;
    }

    private String buildQuery(List<String> namesOfTables, List<List<String>> namesOfColumns, List<String> primaryKeys, Long idToFind) {
        String query = String.format("SELECT ");
        List<String> columnsOfFirstTable = namesOfColumns.get(namesOfColumns.size() - 1);

        for(String columnOfTable: columnsOfFirstTable) {
            if (primaryKeys.get(primaryKeys.size() - 1) == columnOfTable) {
                query += String.format("\"%s\".", namesOfTables.get(namesOfTables.size() - 1));
            }
            query += columnOfTable;
            if(columnsOfFirstTable.indexOf(columnOfTable) != columnsOfFirstTable.size() - 1){
                query += ",";
            }
        }

        for (int i = namesOfTables.size() - 2; i >= 0; i--){
            query += ',';
            List<String> columnsOfCurrentTable = namesOfColumns.get(i);
            for(String columnOfTable: columnsOfCurrentTable) if (primaryKeys.get(i) != columnOfTable){
                query += columnOfTable;
                if(columnsOfCurrentTable.indexOf(columnOfTable) != columnsOfCurrentTable.size() - 1){
                    query += ",";
                }
            }
        }

        query += String.format(" FROM public.\"%s\"", namesOfTables.get(namesOfTables.size() - 1));

        if(namesOfTables.size() > 1) {
            for (int i = namesOfTables.size() - 2; i >= 0; i--){
                query += String.format(" JOIN public.\"%s\" ON public.\"%s\".%s=public.\"%s\".%s",
                        namesOfTables.get(i),
                        namesOfTables.get(namesOfTables.size() - 1),
                        primaryKeys.get(primaryKeys.size() - 1),
                        namesOfTables.get(i),
                        primaryKeys.get(i));
            }
        }

        if (idToFind != null) {
            query += String.format(" WHERE public.\"%s\".%s=%s",
                    namesOfTables.get(namesOfTables.size() - 1),
                    primaryKeys.get(primaryKeys.size() - 1),
                    idToFind);
        }

        return query;
    }

    private List<T> executeQueryAndGetObjects(String query, List<List<Field>> fieldsOfClasses, List<List<String>> namesOfColumns, List<String> primaryKeys) throws SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<Field> valuableFields = this.getListOfValuableFields(fieldsOfClasses);
        List<String> valuableColumns = this.getListOfValuableColumns(namesOfColumns, primaryKeys);
        for(Field field : valuableFields) {
            field.setAccessible(true);
        }
        System.out.println(valuableColumns);
        ResultSet resultSet = this.dataSource.getConnection().prepareStatement(query).executeQuery();
        List<T> resultingList = new ArrayList<>();
        while(resultSet.next()) {
            T resultingObject = clazz.getConstructor().newInstance();
            for(int i = 0; i < valuableFields.size(); i++){
                String currentFieldType = valuableFields.get(i).getType().getName();
                if(currentFieldType.equals("java.lang.Long")){
                    valuableFields.get(i).set(resultingObject, resultSet.getLong(valuableColumns.get(i)));
                } else if(currentFieldType.equals("java.sql.Date")){
                    valuableFields.get(i).set(resultingObject, resultSet.getDate(valuableColumns.get(i)));
                } else if(currentFieldType.equals("java.lang.String")){
                    valuableFields.get(i).set(resultingObject, resultSet.getString(valuableColumns.get(i)));
                } else if(currentFieldType.equals("boolean")){
                    valuableFields.get(i).set(resultingObject, resultSet.getBoolean(valuableColumns.get(i)));
                } else {
                    valuableFields.get(i).set(resultingObject, resultSet.getObject(valuableColumns.get(i)));
                }
            }
            resultingList.add(resultingObject);
        }
        return resultingList;
    }

    private List<Field> getListOfValuableFields(List<List<Field>> fieldsOfClasses) {
        List<Field> listOfValuableFields = new ArrayList<>();
        for(Field field: fieldsOfClasses.get(fieldsOfClasses.size() - 1)) {
            listOfValuableFields.add(field);
        }

        for (int i = fieldsOfClasses.size() - 2; i >= 0; i--) {
            for(Field field: fieldsOfClasses.get(i)) if (field.getAnnotation(PrimaryKey.class) == null) {
                listOfValuableFields.add(field);
            }
        }
        return listOfValuableFields;
    }

    private List<String> getListOfValuableColumns(List<List<String>> namesOfColumns, List<String> primaryKeys) {
        List<String> listOfValuableColumns = new ArrayList<>();
        for(String column: namesOfColumns.get(namesOfColumns.size() - 1)) {
            listOfValuableColumns.add(column);
        }

        for (int i = namesOfColumns.size() - 2; i >= 0; i--) {
            for(String column: namesOfColumns.get(i)) if (column != primaryKeys.get(i)) {
                listOfValuableColumns.add(column);
            }
        }
        return listOfValuableColumns;
    }
}
