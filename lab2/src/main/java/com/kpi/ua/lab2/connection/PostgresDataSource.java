package com.kpi.ua.lab2.connection;
import org.postgresql.ds.PGPoolingDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class PostgresDataSource implements IDataSource{
    private PGPoolingDataSource dataSource;

    public PostgresDataSource() {
        Properties properties = new Properties();
        try {
            properties.load(PostgresDataSource.class.getResourceAsStream("/db.properties"));
            dataSource = new PGPoolingDataSource();
            dataSource.setDatabaseName(properties.getProperty("DB_NAME"));
            dataSource.setServerName(properties.getProperty("DB_HOST"));
            dataSource.setPortNumber(Integer.parseInt(properties.getProperty("DB_PORT")));
            dataSource.setUser(properties.getProperty("DB_USERNAME"));
            dataSource.setPassword(properties.getProperty("DB_PASSWORD"));
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return connection;
    }
}