import java.util.Stack;

public class BufferFile<T> extends FileSystemEntity {
    private final static int MAX_BUF_FILE_SIZE = 20;
    private Stack<T> stack;

    public BufferFile(String name, Directory parent) {
        super(name, parent);
        stack = new Stack<>();
    }

    public void push(T element) {
        this.readwrite(() -> {
            if (stack.size() >= MAX_BUF_FILE_SIZE) {
                throw new IllegalStateException("Buffer overflow");
            }
            stack.push(element);
        });
    }

    public T consume() {
        return this.readwrite(() -> stack.pop());
    }

    @Override
    public FileSystemEntityType getType() {
        return FileSystemEntityType.BUFFER_FILE;
    }
}
