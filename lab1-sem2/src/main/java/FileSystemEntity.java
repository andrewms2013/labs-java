import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public abstract class FileSystemEntity {
    public enum FileSystemEntityType {
        DIRECTORY,
        BINARY_FILE,
        LOG_TEXT_FILE,
        BUFFER_FILE,
        RANDOM_FILE
    }

    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private Directory parent;
    private String name;

    public FileSystemEntity(String name, Directory parent) {
        this.validateName(name);
        this.name = name;

        this.parent = parent;

        if (this.parent != null) {
            if(this.parent.contains(name)) {
                throw new IllegalArgumentException("Parent directory contains file with such name");
            }

            this.parent.addChild(this);
        }
    }

    protected <T> T readonly(Supplier<T> func) {
        readWriteLock.readLock().lock();
        T ret = func.get();
        readWriteLock.readLock().unlock();
        return ret;
    }

    protected void readonly(Runnable func) {
        readWriteLock.readLock().lock();
        func.run();
        readWriteLock.readLock().unlock();
    }

    protected <T> T readwrite(Supplier<T> func) {
        readWriteLock.writeLock().lock();
        T ret = func.get();
        readWriteLock.writeLock().unlock();
        return ret;
    }

    protected void readwrite(Runnable func) {
        readWriteLock.writeLock().lock();
        func.run();
        readWriteLock.writeLock().unlock();
    }

    protected void setName(String name) {
        this.readwrite(() -> {
            this.validateName(name);
            String oldName = this.name;
            this.name = name;
            if (parent != null) {
                parent.updateChild(this, oldName);
            }
        });
    }

    protected Directory getRoot() {

        return this.readonly(() -> {
            if (parent == null) {
                return (Directory) this;
            }
            Directory current = parent;
            while (current.getParent() != null) {
                current = current.getParent();
            }
            return current;
        });

    }

    public Directory getParent() {
        return this.readonly(() -> this.parent);
    }

    public void delete() {
        this.readwrite(() -> {
            if (parent != null) {
                parent.removeChild(name);
            }
            parent = null;
        });
    }

    public void move(Directory destination) {
        this.readwrite(() -> {
            if (destination == null) {
                throw new IllegalArgumentException("Destination could not be null");
            }
            if (parent != null) {
                parent.removeChild(name);
            }
            parent = destination;
            parent.addChild(this);
        });
    }

    public String getName() {
        return this.readonly(() -> name);
    }

    public abstract FileSystemEntityType getType();

    private void validateName(String name) {
        if(name == null || name == "") {
            throw new IllegalArgumentException("Invalid name. Name can not be empty");
        }
    }
}
