public class RandomFile extends FileSystemEntity {
    public RandomFile(String name, Directory parent) {
        super(name, parent);
    }

    public Double getData() {
        return Math.random();
    }

    @Override
    public FileSystemEntityType getType() {
        return FileSystemEntityType.RANDOM_FILE;
    }
}
