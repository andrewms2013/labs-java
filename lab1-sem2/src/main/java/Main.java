import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

class Thread1 extends ThreadTask {
    public Thread1(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() throws InterruptedException {
        System.out.println("Thread 1 init");
        var dir = new Directory("thread1Dir", root);

        for (int i = 0; i < 5; i++) {
            new BufferFile("test" + i, root);
            Thread.sleep(100);
        }
    }

    public void task() throws InterruptedException {
        System.out.println("Thread 1 task");
        for (int i = 0; i < 5; i++) {
            BufferFile file = (BufferFile) root.getChild("test" + i);
            file.move((Directory) root.getChild("lvl1"));
            Thread.sleep(100);
        }
    }

    public void task_finalize() {
        System.out.println("Thread 1 finalize");
    }
}

class Thread2 extends ThreadTask {
    public Thread2(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() throws InterruptedException {
        System.out.println("Thread 2 init");
        Thread.sleep(1000);
        new LogTextFile("LogTextFile1", root, "nono");
    }

    public void task() {
        System.out.println("Thread 2 task");
    }

    public void task_finalize() {
        System.out.println("Thread 2 finalize");
    }
}

class Thread3 extends ThreadTask {
    public Thread3(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            barrier.await();
            task_finalize();
            barrier.await();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void task_init() {
        System.out.println("Thread 3 init");
    }

    public void task() throws InterruptedException {
        System.out.println("Thread 3 task");
        Thread.sleep(1050);
        ((LogTextFile) root.getChild("LogTextFile1")).addLine("line");
    }

    public void task_finalize() {
        System.out.println("Thread 3 finalize");
    }
}

class Thread4 extends ThreadTask {
    public Thread4(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            barrier.await();
            task_finalize();
            barrier.await();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void task_init() {
        System.out.println("Thread 4 init");
    }


    public void task() throws InterruptedException {
        System.out.println("Thread 4 task");
        Thread.sleep(200);
        ((LogTextFile) root.getChild("LogTextFile1")).addLine("nono");
    }

    public void task_finalize() {
        System.out.println("Thread 4 finalize");
        ((LogTextFile) root.getChild("LogTextFile1")).addLine("nono");
        System.out.println(((LogTextFile) root.getChild("LogTextFile1")).getData());
    }
}

class Thread5 extends ThreadTask {
    @Override
    public void run() {
        try {
            barrier.await();
            barrier.await();
            barrier.await();
            task_init();
            task();
            task_finalize();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Thread5(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() {
        System.out.println("Thread 5 init");
    }

    public void task() {
        System.out.println("Thread 5 task");
    }

    public void task_finalize() {
        System.out.println("Thread 5 finalize");
    }
}

public class Main {
    public static void main(String[] args) throws BrokenBarrierException, InterruptedException {
        var root = new Directory("root", null);
        var lvlOneDir = new Directory("lvl1", root);
        var binaryFile = new BinaryFile("binary", lvlOneDir, "data".getBytes());
        var bufferFile = new BufferFile<Integer>("buffer", lvlOneDir);
        var logTextFile = new LogTextFile("logTextFile", lvlOneDir, "data");
        var randomFile = new RandomFile("U know", lvlOneDir);

        CyclicBarrier barrier = new CyclicBarrier(6);

        new Thread(new Thread1(root, barrier), "1").start();
        new Thread(new Thread2(root, barrier), "2").start();
        new Thread(new Thread3(root, barrier), "3").start();
        new Thread(new Thread4(root, barrier), "4").start();
        new Thread(new Thread5(root, barrier), "5").start();

        barrier.await();
        barrier.await();
        barrier.await();
    }
}
