import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Directory extends FileSystemEntity {
    private static final int DIR_MAX_ELEMS = 50;
    private Map<String, FileSystemEntity> children = new HashMap<>(DIR_MAX_ELEMS);

    public Directory(String name, Directory parent) {
        super(name, parent);
    }

    protected boolean contains(String childName) {
        return this.readonly(() -> children.containsKey(childName));
    }

    protected int countChildren() {
        return this.readonly(() -> children.size());
    }

    protected FileSystemEntity getChild(String childName) {
        return this.readonly(() -> children.get(childName));
    }

    protected void addChild(FileSystemEntity child) {
        this.readwrite(() -> {
            if (countChildren() >= DIR_MAX_ELEMS) {
                throw new IllegalStateException("Directory can't contain more than 50 children");
            }
            children.put(child.getName(), child);
        });
    }

    protected FileSystemEntity removeChild(String childName) {
        return this.readwrite(() -> children.remove(childName));
    }

    protected void updateChild(FileSystemEntity child, String oldName) {
        this.readwrite(() -> {
            children.put(child.getName(), removeChild(oldName));
        });
    }

    protected List<FileSystemEntity> getChildren() {
        return this.readonly(() -> new ArrayList<>(children.values()));
    }

    public List<FileSystemEntity> list() {
        return getChildren();
    }

    @Override
    public void move(Directory destination) {
        if (this.getParent() == null) {
            throw new IllegalStateException("Can't move root directory");
        }
        super.move(destination);
    }

    @Override
    public FileSystemEntityType getType() {
        return FileSystemEntityType.DIRECTORY;
    }
}