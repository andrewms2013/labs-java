public class LogTextFile extends FileSystemEntity {
    private StringBuffer data;

    public LogTextFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = new StringBuffer(data);
    }

    public String getData() {
        return this.readonly(() -> data.toString());
    }

    public void addLine(String line) {
        this.readwrite(() -> {
            if (line == null) {
                throw new IllegalArgumentException("Line should not be null");
            }
            data.append(line);
        });
    }

    @Override
    public FileSystemEntityType getType() {
        return FileSystemEntityType.LOG_TEXT_FILE;
    }
}
