import java.util.concurrent.CyclicBarrier;

abstract class ThreadTask implements Runnable {
    Directory root;
    protected CyclicBarrier barrier;

    public ThreadTask(Directory root, CyclicBarrier barrier) {
        this.root = root;
        this.barrier = barrier;
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            barrier.await();
            barrier.await();
            task_finalize();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void task_init() throws InterruptedException {}

    public void task() throws InterruptedException {}

    public void task_finalize() {}
}
