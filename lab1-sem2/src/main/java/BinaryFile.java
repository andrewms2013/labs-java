public class BinaryFile extends FileSystemEntity {
    private byte[] data;

    public BinaryFile(String name, Directory parent, byte[] data) {
        super(name, parent);
        if(data == null) {
            throw new IllegalArgumentException("binary data can't be null");
        }
        this.data = data;
    }

    public byte[] read() {
        return this.readonly(() -> data.clone());
    }

    @Override
    public FileSystemEntityType getType() {
        return FileSystemEntityType.BINARY_FILE;
    }
}
