import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Log text file")
public class LogTextFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        var file = new LogTextFile("file", null, "test");
        assertNotNull(file);
        assertEquals(FileSystemEntity.FileSystemEntityType.LOG_TEXT_FILE, file.getType());
        assertEquals("file", file.getName());
    }

    @Test
    @DisplayName("Get add data")
    void getAddDataTest() {
        var file = new LogTextFile("file", null, "test");

        assertEquals("test", file.getData());
        file.addLine("\ntest");
        assertEquals("test\ntest", file.getData());
        file.addLine("");
        assertEquals("test\ntest", file.getData());
    }
}
