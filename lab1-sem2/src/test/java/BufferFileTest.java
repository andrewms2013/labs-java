import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Buffer file")
public class BufferFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        var file = new BufferFile<Integer>("test", null);
        assertNotNull(file);
        assertEquals(FileSystemEntity.FileSystemEntityType.BUFFER_FILE, file.getType());
        assertEquals("test", file.getName());
    }

    @Test
    @DisplayName("Consume")
    void consumeTest() {
        var file = new BufferFile<Integer>("test", null);

        assertThrows(EmptyStackException.class, file::consume);
        file.push(1);
        file.push(2);
        assertEquals(new Integer(2), file.consume());
        assertEquals(new Integer(1), file.consume());
        assertThrows(EmptyStackException.class, file::consume);
    }

    @Test
    @DisplayName("Buffer file overflow")
    void bufferFileOverflowTest() {
        var file = new BufferFile<Integer>("file", null);

        for (int i = 0; i < 20; i++) {
            file.push(i);
        }
        assertThrows(IllegalStateException.class, () -> file.push(21));
    }
}
