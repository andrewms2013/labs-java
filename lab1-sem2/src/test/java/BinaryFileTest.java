import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Binary file")
public class BinaryFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        var dir = new Directory("directory", null);
        assertThrows(IllegalArgumentException.class, () -> new BinaryFile("", dir, "test".getBytes()));

        var file = new BinaryFile("file", dir, "test".getBytes());
        assertNotNull(file);
        assertEquals(FileSystemEntity.FileSystemEntityType.BINARY_FILE, file.getType());
        assertEquals("file", file.getName());
    }

    @Test
    @DisplayName("Read bytes")
    void readBytesTest() {
        var file = new BinaryFile("f", null, "test".getBytes());
        assertArrayEquals("test".getBytes(), file.read());
    }
}