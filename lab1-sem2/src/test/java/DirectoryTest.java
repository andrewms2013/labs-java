import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Directory")
public class DirectoryTest {
    @Test
    @DisplayName("Create directory")
    void createDirectoryTest() {
        assertThrows(IllegalArgumentException.class, () -> new Directory("", null));

        var dir = new Directory("test", null);
        assertNotNull(dir);

        assertEquals(FileSystemEntity.FileSystemEntityType.DIRECTORY, dir.getType());
        assertEquals("test", dir.getName());

        var subDir = new Directory("subDir", dir);
        assertEquals("subDir", subDir.getName());
    }

    @Test
    @DisplayName("Get directory children")
    void getDirectoryChildrenTest() {
        var root = new Directory("dir", null);
        var listElements = new ArrayList<FileSystemEntity>();
        listElements.add(new Directory("sub", root));
        listElements.add(new BinaryFile("file.bin", root, "data".getBytes()));
        listElements.add(new LogTextFile("log", root, "line"));
        assertEquals(listElements, root.list());
        assertEquals(Collections.emptyList(), new Directory("dir", null).list());
    }


    @Test
    @DisplayName("Move directory")
    void moveSubdirectoriesAndFilesTest() {
        var root = new Directory("dir", null);
        var subDir = new Directory("folder", root);
        var anotherSubDir = new Directory("dir", root);
        var log = new LogTextFile("l.txt", subDir, "\n");

        assertThrows(IllegalStateException.class, () -> root.move(subDir));
        assertDoesNotThrow(() -> anotherSubDir.move(subDir));
        assertTrue(subDir.list().contains(anotherSubDir));
        assertTrue(subDir.list().contains(log));
    }

    @Test
    @DisplayName("Directory overflow")
    void directoryOverflowTest() {
        var root = new Directory("dir", null);

        for (int i = 0; i < 50; i++) {
            new BufferFile("test" + i, root);
        }
        assertEquals(50, root.list().size());
        assertThrows(IllegalStateException.class, () -> new BufferFile("test", root));
    }
}