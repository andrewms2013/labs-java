import funcset.*;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.Function;

public class FunctionalSetTest {
    public FunctionalSetTest() {}

    @Test
    public void emptySet() {
        PurelyFunctionalSet<Integer> emptySet = FunctionalSetHelper.empty();
        for (int i = -1000; i <= 1000; i++) {
            Assert.assertFalse(emptySet.contains(i));
        }
    }

    @Test
    public void singeltonSet() {
        Double dbl = 2.1;
        PurelyFunctionalSet<Double> doubleSet = FunctionalSetHelper.singeltonSet(dbl);
        Assert.assertFalse(doubleSet.contains(2.2));
        Assert.assertTrue(doubleSet.contains(2.1));
    }

    @Test
    public void union() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);

        PurelyFunctionalSet<Integer> unionSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);

        Assert.assertTrue(unionSet.contains(1));
        Assert.assertTrue(unionSet.contains(2));
        Assert.assertFalse(unionSet.contains(3));
    }

    @Test
    public void intercept() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);
        PurelyFunctionalSet<Integer> integerThreeSet = FunctionalSetHelper.singeltonSet(3);
        PurelyFunctionalSet<Integer> unionOneTwoSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);
        PurelyFunctionalSet<Integer> unionTwoThreeSet = FunctionalSetHelper.union(integerTwoSet, integerThreeSet);
        PurelyFunctionalSet<Integer> intersectSet = FunctionalSetHelper.intersect(unionOneTwoSet, unionTwoThreeSet);

        Assert.assertTrue(intersectSet.contains(2));
        Assert.assertFalse(intersectSet.contains(1));
    }

    @Test
    public void diff() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);
        PurelyFunctionalSet<Integer> integerThreeSet = FunctionalSetHelper.singeltonSet(3);
        PurelyFunctionalSet<Integer> unionOneTwoSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);
        PurelyFunctionalSet<Integer> unionTwoThreeSet = FunctionalSetHelper.union(integerTwoSet, integerThreeSet);
        PurelyFunctionalSet<Integer> diffSet = FunctionalSetHelper.diff(unionOneTwoSet, unionTwoThreeSet);

        Assert.assertTrue(diffSet.contains(1));
        Assert.assertTrue(diffSet.contains(3));
        Assert.assertFalse(diffSet.contains(2));
    }

    @Test
    public void filter() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);
        PurelyFunctionalSet<Integer> integerThreeSet = FunctionalSetHelper.singeltonSet(3);
        PurelyFunctionalSet<Integer> integerFourSet = FunctionalSetHelper.singeltonSet(4);
        PurelyFunctionalSet<Integer> unionOneTwoSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);
        PurelyFunctionalSet<Integer> unionThreeFourSet = FunctionalSetHelper.union(integerThreeSet, integerFourSet);
        PurelyFunctionalSet<Integer> unionAll = FunctionalSetHelper.union(unionOneTwoSet, unionThreeFourSet);

        Predicate<Integer> pred = x -> x < 3;

        PurelyFunctionalSet<Integer> filteredSet = FunctionalSetHelper.filter(unionAll, pred);

        Assert.assertTrue(filteredSet.contains(1));
        Assert.assertTrue(filteredSet.contains(2));
        Assert.assertFalse(filteredSet.contains(3));
        Assert.assertFalse(filteredSet.contains(4));
    }

    @Test
    public void forAll() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);
        PurelyFunctionalSet<Integer> integerThreeSet = FunctionalSetHelper.singeltonSet(3);
        PurelyFunctionalSet<Integer> integerFourSet = FunctionalSetHelper.singeltonSet(4);
        PurelyFunctionalSet<Integer> unionOneTwoSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);
        PurelyFunctionalSet<Integer> unionThreeFourSet = FunctionalSetHelper.union(integerThreeSet, integerFourSet);
        PurelyFunctionalSet<Integer> unionFourSet = FunctionalSetHelper.union(unionOneTwoSet, unionThreeFourSet);

        Predicate<Integer> pred = x -> x < 5;

        Assert.assertTrue(FunctionalSetHelper.forAll(unionFourSet, pred));

        PurelyFunctionalSet<Integer> integerFiveSet = FunctionalSetHelper.singeltonSet(5);
        PurelyFunctionalSet<Integer> unionFiveSet = FunctionalSetHelper.union(unionFourSet, integerFiveSet);
        Assert.assertFalse(FunctionalSetHelper.forAll(unionFiveSet, pred));
    }

    @Test
    public void exists() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);
        PurelyFunctionalSet<Integer> integerThreeSet = FunctionalSetHelper.singeltonSet(3);
        PurelyFunctionalSet<Integer> integerFourSet = FunctionalSetHelper.singeltonSet(4);
        PurelyFunctionalSet<Integer> unionOneTwoSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);
        PurelyFunctionalSet<Integer> unionThreeFourSet = FunctionalSetHelper.union(integerThreeSet, integerFourSet);
        PurelyFunctionalSet<Integer> unionFourSet = FunctionalSetHelper.union(unionOneTwoSet, unionThreeFourSet);

        Predicate<Integer> pred = x -> x < 2;

        Assert.assertTrue(FunctionalSetHelper.exists(unionFourSet, pred));

        Predicate<Integer> pred2 = x -> x < 1;

        Assert.assertFalse(FunctionalSetHelper.exists(unionFourSet, pred2));
    }

    @Test
    public void map() {
        PurelyFunctionalSet<Integer> integerOneSet = FunctionalSetHelper.singeltonSet(1);
        PurelyFunctionalSet<Integer> integerTwoSet = FunctionalSetHelper.singeltonSet(2);
        PurelyFunctionalSet<Integer> integerThreeSet = FunctionalSetHelper.singeltonSet(3);
        PurelyFunctionalSet<Integer> integerFourSet = FunctionalSetHelper.singeltonSet(4);
        PurelyFunctionalSet<Integer> unionOneTwoSet = FunctionalSetHelper.union(integerOneSet, integerTwoSet);
        PurelyFunctionalSet<Integer> unionThreeFourSet = FunctionalSetHelper.union(integerThreeSet, integerFourSet);
        PurelyFunctionalSet<Integer> unionFourSet = FunctionalSetHelper.union(unionOneTwoSet, unionThreeFourSet);

        Function<Integer, Integer> mapper = x -> x + 5;

        PurelyFunctionalSet<Integer> mapSet = FunctionalSetHelper.map(unionFourSet, mapper);


        Assert.assertFalse(mapSet.contains(1));
        Assert.assertTrue(mapSet.contains(9));
        Assert.assertFalse(mapSet.contains(25));
    }
}