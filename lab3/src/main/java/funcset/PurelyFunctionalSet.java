package funcset;

@FunctionalInterface
public interface PurelyFunctionalSet<T> {
    boolean contains(T Element);
}
