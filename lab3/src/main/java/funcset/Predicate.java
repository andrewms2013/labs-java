package funcset;

@FunctionalInterface
public interface Predicate<T> {
    boolean test(T Element);
}

