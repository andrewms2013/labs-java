package funcset;

import java.util.function.Function;

public class FunctionalSetHelper {
    public static<T>PurelyFunctionalSet<T> empty() {
        return x -> false;
    }

    public static <T>PurelyFunctionalSet<T> singeltonSet(T val) {
        return x -> x.equals(val);
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> s,
                                                   PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) || t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> s,
                                                       PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> s,
                                                  PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) ^ t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> s,
                                                    Predicate<T> p) {
        return x -> s.contains(x) && p.test(x);
    }

    public static <T> boolean forAll(PurelyFunctionalSet<Integer> s,
                                     Predicate<Integer> p) {
          return forAllRecursion(s, p, 1000, -1000);
    }

    private static <T> boolean forAllRecursion(PurelyFunctionalSet<Integer> s, Predicate<Integer> p, Integer n, Integer i) {
        if(n <= i) {
            return true;
        }
        if(s.contains(i) && !p.test(i)) {
            return false;
        }
        return forAllRecursion(s, p, n, ++i);
    }

    public static <T> boolean exists(PurelyFunctionalSet<Integer> s,
                                     Predicate<Integer> p) {
        return !forAll(s, x -> !p.test(x));
    }

    public static <R> PurelyFunctionalSet<R> map(PurelyFunctionalSet<Integer> s,
                                  Function<Integer, R> p) {
        return x -> exists(s, y -> p.apply(y) == x);
    }


}
